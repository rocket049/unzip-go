module unzip-go

go 1.14

require (
	github.com/yeka/zip v0.0.0-20180914125537-d046722c6feb
	golang.org/x/crypto v0.0.0-20220314234724-5d542ad81a58 // indirect
	golang.org/x/text v0.3.6
)
